package com.gois;

public class Main {

    public static void main(String[] args) {

        // With a decimal point, Java will assume that is a double
        float myFloatValue = (float)5.4;
        // 32 bits
        float myOtherFloatValue = 5.4f / 2;
        // 64 bits
        double myDouble = 5.4d;

        System.out.println("myOtherFloatValue: " + myOtherFloatValue);
    }
}
