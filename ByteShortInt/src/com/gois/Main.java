package com.gois;

public class Main {

    public static void main(String[] args) {

        // 8 bits
        byte myMinByteValue = -128;                         // - 2 ^ 7
        byte myMaxByteValue = 127;                          //   2 ^ 7 - 1
        // If we don't put the cast, we'll have a compilation error. Java automatically converts an expression
        // to integer by default
        byte myNewByteValue = (byte) (myMaxByteValue / 2);

        // 16 bits
        short myMinShortValue = -32768;                     // - 2 ^ 15
        short myMaxShortValue = 32767;                      //   2 ^ 15 - 1

        // 32 bits
        int myMinIntValue = -2_147_483_648;                 // - 2 ^ 31
        int myMaxIntValue = 2_147_483_647;                  //   2 ^ 31 - 1

        // 64 bits
        long myMinLongValue = -9_223_372_036_854_775_808L;   // - 2 ^ 63
        long myMaxLongValue = 9_223_372_036_854_775_807L;    // - 2 ^ 63

    }
}
